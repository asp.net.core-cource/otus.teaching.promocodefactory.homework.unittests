﻿using System;
using System.Collections.Generic;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;
using Otus.Teaching.PromoCodeFactory.WebHost.Services.Exceptions;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class CancelPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IPartnersService> _partnersService;
        private readonly PartnersController _partnersController;
        private readonly Guid _id;

        public CancelPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersService = fixture.Freeze<Mock<IPartnersService>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
            _id = Guid.NewGuid();
        }

        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }
        
        [Fact]
        public async void CancelPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            _partnersService.Setup(s => s.CancelPartnerPromoCodeLimitAsync(It.IsAny<Guid>()))
                .ThrowsAsync(new PartnerNotFoundException("Партнер не найден"));

            // Act
            var act = await _partnersController.CancelPartnerPromoCodeLimitAsync(_id);

            // Assert
            act.Should().BeAssignableTo<NotFoundResult>();
        }
        
        [Fact]
        public async void CancelPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            _partnersService.Setup(s => s.CancelPartnerPromoCodeLimitAsync(It.IsAny<Guid>())).
                ThrowsAsync(new PartnerNotActiveException("Данный партнер не активен"));

            // Act
            var act = await _partnersController.CancelPartnerPromoCodeLimitAsync(_id);

            // Assert
            act.Should().BeAssignableTo<BadRequestObjectResult>();
        }
    }
}