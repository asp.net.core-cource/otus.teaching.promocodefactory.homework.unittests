﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;
using Otus.Teaching.PromoCodeFactory.WebHost.Services.Exceptions;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
	public class SetPartnerPromoCodeLimitAsyncControllerTests
	{
		private readonly Mock<IPartnersService> _partnersService;
		private readonly PartnersController _partnersController;
		private readonly SetPartnerPromoCodeLimitRequest _request;

		private readonly Guid _id;

		public SetPartnerPromoCodeLimitAsyncControllerTests()
		{
			var fixture = new Fixture().Customize(new AutoMoqCustomization());
			_partnersService = fixture.Freeze<Mock<IPartnersService>>();
			_partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();

			_id = Guid.NewGuid();

			var builder = new SetPartnerPromoCodeLimitRequestBuilder();
			builder.SetEndDate();
			builder.SetLimit();
			_request = builder.BuildSetPartnerPromoCodeLimitRequest();
		}

		//Если партнер не найден, то также нужно выдать ошибку 404;
		[Fact]
		public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_Returns404()
		{
			// Arrange
			_partnersService.Setup(s => s.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), It.IsAny<DateTime>(), It.IsAny<int>()))
				.ThrowsAsync(new PartnerNotFoundException("Партнер не найден"));

			// Act
			var act = await _partnersController.SetPartnerPromoCodeLimitAsync(_id, _request);

			// Assert
			act.Should().BeOfType<NotFoundResult>().Subject.StatusCode.Should().Be(404);
		}

		// Если партнер заблокирован, то есть поле IsActive = false в классе Partner,
		// то также нужно выдать ошибку 400;
		[Fact]
		public async void SetPartnerPromoCodeLimitAsync_PartnerNotActiveException_Returns400()
		{
			// Arrange
			_partnersService.Setup(s => s.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), It.IsAny<DateTime>(), It.IsAny<int>()))
				.ThrowsAsync(new PartnerNotActiveException("Данный партнер не активен"));

			// Act
			var act = await _partnersController.SetPartnerPromoCodeLimitAsync(_id, _request);

			// Assert
			act.Should().BeOfType<BadRequestObjectResult>().Subject.StatusCode.Should().Be(400);
		}

		[Fact]
		public async void SetPartnerPromoCodeLimitAsync_WrongLimitException_Returns400()
		{
			// Arrange
			_partnersService.Setup(s => s.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), It.IsAny<DateTime>(), It.IsAny<int>()))
				.ThrowsAsync(new WrongLimitException("Лимит должен быть больше 0"));

			// Act
			var act = await _partnersController.SetPartnerPromoCodeLimitAsync(_id, _request);

			// Assert
			act.Should().BeOfType<BadRequestObjectResult>().Subject.StatusCode.Should().Be(400);
		}

	}
}
