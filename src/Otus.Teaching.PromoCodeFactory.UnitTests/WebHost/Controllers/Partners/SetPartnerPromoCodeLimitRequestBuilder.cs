﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
	internal class SetPartnerPromoCodeLimitRequestBuilder
	{
		private SetPartnerPromoCodeLimitRequest _request;
		public SetPartnerPromoCodeLimitRequestBuilder()
		{
			_request = new SetPartnerPromoCodeLimitRequest();
		}
		internal void SetEndDate()
		{
			_request.EndDate = DateTime.Now.AddDays(2);
		}
		internal void SetLimit()
		{
			_request.Limit = 10;
		}

		internal SetPartnerPromoCodeLimitRequest BuildSetPartnerPromoCodeLimitRequest()
		{
			return _request;
		}
	}
}
