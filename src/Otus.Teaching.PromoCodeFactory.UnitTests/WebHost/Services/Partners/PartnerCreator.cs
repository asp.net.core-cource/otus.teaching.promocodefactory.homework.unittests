﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Services.Partners
{
	internal static class PartnerCreator
	{
		public static Partner CreateNullPartner()
		{
			return null;
		}
		public static Partner CreateNotActivePartner()
		{
			return new Partner { IsActive = false };
		}
		public static Partner CreateActivePartnerWithActiveLimit()
		{
			return new Partner
			{
				IsActive = true,
				PartnerLimits = new List<PartnerPromoCodeLimit> { new PartnerPromoCodeLimit { Id = Guid.NewGuid()} },
				NumberIssuedPromoCodes = 10
			};
		}
		public static Partner CreateActivePartnerWithNoActiveLimits()
		{
			return new Partner
			{
				IsActive = true,
				PartnerLimits = new List<PartnerPromoCodeLimit> { new PartnerPromoCodeLimit { CancelDate = System.DateTime.Now} },
				NumberIssuedPromoCodes = 10
			};
		}
	}
}
