﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;
using Otus.Teaching.PromoCodeFactory.WebHost.Services.Exceptions;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Services.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly IPartnersService _partnersService;
        private readonly Guid _partnerId;
        private readonly DateTime _datetime;
        private readonly int _limit;
        public SetPartnerPromoCodeLimitAsyncTests()
		{
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersService = fixture.Build<PartnersService>().OmitAutoProperties().Create();

            _partnerId = Guid.NewGuid();
            _datetime = DateTime.Now.AddDays(2);
            _limit = 10;
        }

        // Если партнер не найден, то также нужно выдать ошибку PartnerNotFoundException (404 в дальнейшем)
        [Fact]
        public void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsPartnerNotFoundException()
        {
            // Arrange
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(PartnerCreator.CreateNullPartner());

            // Act
            Func<Task> act = async () => await _partnersService.SetPartnerPromoCodeLimitAsync(_partnerId, _datetime, _limit);

            // Assert
            act.Should().Throw<PartnerNotFoundException>().WithMessage("Партнер не найден");
        }

        // Если партнер заблокирован, то есть поле IsActive = false в классе Partner,
        // то также нужно выдать ошибку PartnerNotActiveException (400 в дальнейшем)
        [Fact]
        public void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsPartnerNotActiveException()
        {
            // Arrange
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(PartnerCreator.CreateNotActivePartner());

            // Act
            Func<Task> act = async () => await _partnersService.SetPartnerPromoCodeLimitAsync(_partnerId, _datetime, _limit);

            // Assert
            act.Should().Throw<PartnerNotActiveException>().WithMessage("Данный партнер не активен");
        }

        // Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал;
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerWithActiveLimit_LimitShouldBeReset()
        {
            // Arrange
            var partner = PartnerCreator.CreateActivePartnerWithActiveLimit();
            var expectedNumberIssuedPromoCodes = 0;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            // Act
            await _partnersService.SetPartnerPromoCodeLimitAsync(_partnerId, _datetime, _limit);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(expectedNumberIssuedPromoCodes);
        }

        // Если партнеру выставляется лимит, и у него нет активных лимитов, то количество промокодов,
        // которые партнер выдал, не обнуляется
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerWithNoActiveLimits_LimitShouldNotBeReset()
        {
            // Arrange
            var partner = PartnerCreator.CreateActivePartnerWithNoActiveLimits();
            var expectedNumberIssuedPromoCodes = partner.NumberIssuedPromoCodes;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            // Act
            await _partnersService.SetPartnerPromoCodeLimitAsync(_partnerId, _datetime, _limit);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(expectedNumberIssuedPromoCodes);
        }

        // При установке лимита нужно отключить предыдущий лимит;
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerWithActiveLimit_PreviousLimitShouldBeOff()
        {
            // Arrange
            var partner = PartnerCreator.CreateActivePartnerWithActiveLimit();
            var limitGuid = partner.PartnerLimits.Select(lim => lim.Id).Single();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            // Act
            await _partnersService.SetPartnerPromoCodeLimitAsync(_partnerId, _datetime, _limit);

            // Assert
            partner.PartnerLimits.Should().ContainSingle(lim => lim.Id == limitGuid && lim.CancelDate.HasValue);
            partner.PartnerLimits.Should().ContainSingle(lim => !lim.CancelDate.HasValue);
        }

        // Лимит должен быть больше 0;
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_TrySetNewLimitLessZero_ShouldBeWrongLimitException()
        {
            // Arrange
            var partner = PartnerCreator.CreateActivePartnerWithActiveLimit();
            var newlimitLessZero = -1;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            // Act
            Func<Task> act = async () => await _partnersService.SetPartnerPromoCodeLimitAsync(_partnerId, _datetime, newlimitLessZero);

            // Assert
            act.Should().Throw<WrongLimitException>().WithMessage("Лимит должен быть больше 0");

        }

        // Нужно убедиться, что сохранили новый лимит в базу данных(это нужно проверить Unit-тестом);
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_TrySetNewLimit_NewLimitShouldBeSet()
        {
            // Arrange
            var partner = PartnerCreator.CreateActivePartnerWithActiveLimit();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            // Act
            await _partnersService.SetPartnerPromoCodeLimitAsync(_partnerId, _datetime, _limit);

            // Assert
            _partnersRepositoryMock.Verify(x => x.UpdateAsync(It.IsAny<Partner>()),Times.Once());
        }
    }
}