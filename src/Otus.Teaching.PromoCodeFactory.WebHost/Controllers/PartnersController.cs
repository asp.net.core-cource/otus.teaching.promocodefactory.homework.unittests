﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;
using Otus.Teaching.PromoCodeFactory.WebHost.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.WebHost.Services.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Партнеры
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PartnersController
        : ControllerBase
    {
        private readonly IPartnersService _partnersService;

        public PartnersController(IPartnersService partnersService, IRepository<Partner> partnersRepository)
        {
            _partnersService = partnersService;
        }

        [HttpGet]
        public async Task<ActionResult<List<PartnerResponse>>> GetPartnersAsync()
        {
            var partners = await _partnersService.GetPartnersAsync();

            var response = partners.Select(x => new PartnerResponse()
            {
                Id = x.Id,
                Name = x.Name,
                NumberIssuedPromoCodes = x.NumberIssuedPromoCodes,
                IsActive = true,
                PartnerLimits = x.PartnerLimits
                    .Select(y => new PartnerPromoCodeLimitResponse()
                    {
                        Id = y.Id,
                        PartnerId = y.PartnerId,
                        Limit = y.Limit,
                        CreateDate = y.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        EndDate = y.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        CancelDate = y.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss"),
                    }).ToList()
            });

            return Ok(response);
        }
        
        [HttpGet("{id}/limits/{limitId}")]
        public async Task<ActionResult<PartnerPromoCodeLimit>> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            var limit = await _partnersService.GetPartnerLimitAsync(id, limitId);

            var response = new PartnerPromoCodeLimitResponse()
            {
                Id = limit.Id,
                PartnerId = limit.PartnerId,
                Limit = limit.Limit,
                CreateDate = limit.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
                EndDate = limit.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
                CancelDate = limit.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss"),
            };
            
            return Ok(response);
        }
        
        [HttpPost("{id}/limits")]
        public async Task<IActionResult> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequest request)
        {
			try
			{
                (Guid partnerId, Guid newLimitId) ids = await _partnersService.SetPartnerPromoCodeLimitAsync(id, request.EndDate, request.Limit);

                return CreatedAtAction(nameof(GetPartnerLimitAsync), new { id = ids.partnerId, limitId = ids.newLimitId }, null);
            }
			catch(PartnerNotFoundException)
			{
                return NotFound();
            }
            catch(PartnerNotActiveException ex)
            {
                return BadRequest(ex.Message);
            }
            catch(WrongLimitException ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        [HttpPost("{id}/canceledLimits")]
        public async Task<IActionResult> CancelPartnerPromoCodeLimitAsync(Guid id)
        {
			try
			{
                await _partnersService.CancelPartnerPromoCodeLimitAsync(id);

                return NoContent();
            }
            catch(PartnerNotFoundException)
            {
                return NotFound();
            }
            catch(PartnerNotActiveException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}