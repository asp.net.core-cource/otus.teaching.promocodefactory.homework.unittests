﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Otus.Teaching.PromoCodeFactory.WebHost.Services.Exceptions;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Middlewares
{
    public class ExceptionHandleMiddleware
    {
        private readonly RequestDelegate _next;
        public ExceptionHandleMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(
            HttpContext httpContext,
            IWebHostEnvironment environment)
        {
            httpContext.Request.EnableBuffering();

            try
            {
                await _next(httpContext);
            }
            catch(Exception ex)
            {
                await HandleExceptionAsync(httpContext, environment, ex);
            }
        }

        private Task HandleExceptionAsync(
            HttpContext httpContext,
            IWebHostEnvironment environment,
            Exception ex)
        {
            var exceptionGuid = Guid.NewGuid();

            var responseErrorMessage = environment.IsProduction()
                ? exceptionGuid.ToString()
                : ex.Message;
            var errorResponse = JsonConvert.SerializeObject(new { Error = responseErrorMessage });

            httpContext.Response.ContentType = "application/json";

            if(ex is PartnerNotFoundException)
                httpContext.Response.StatusCode = (int)HttpStatusCode.NotFound;
            else
                httpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;

            return httpContext.Response.WriteAsync(errorResponse);
        }
    }

    public static class ExceptionHandleExtension
    {
        public static void UseExceptionHandle(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionHandleMiddleware>();
        }

    }
}
