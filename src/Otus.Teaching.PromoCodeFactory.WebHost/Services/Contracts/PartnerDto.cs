﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services.Contracts
{
	public class PartnerDto
	{
		public Guid Id { get; set; }
		public string Name { get; set; }

		public int NumberIssuedPromoCodes { get; set; }

		public bool IsActive { get; set; }

		public ICollection<PartnerPromoCodeLimitDto> PartnerLimits { get; set; }
	}
}
