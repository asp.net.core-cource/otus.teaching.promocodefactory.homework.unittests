﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services.Exceptions
{
	public class PartnerNotActiveException : Exception
	{
		public PartnerNotActiveException(string message, Exception ex = null) : base(message, ex)
		{

		}
	}
}
