﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services.Exceptions
{
	public class PartnerNotFoundException : Exception
	{
		public PartnerNotFoundException(string message, Exception ex = null) : base(message, ex)
		{

		}
	}
}
