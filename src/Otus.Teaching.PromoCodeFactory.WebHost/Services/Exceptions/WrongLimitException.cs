﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services.Exceptions
{
	public class WrongLimitException : Exception
	{
		public WrongLimitException(string message, Exception ex = null) : base(message, ex)
		{

		}
	}
}
