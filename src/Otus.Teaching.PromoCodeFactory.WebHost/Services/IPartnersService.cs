﻿using Otus.Teaching.PromoCodeFactory.WebHost.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
	public interface IPartnersService
	{
		public Task<List<PartnerDto>> GetPartnersAsync();
		public Task<PartnerPromoCodeLimitDto> GetPartnerLimitAsync(Guid id, Guid limitId);
		public Task<(Guid, Guid)> SetPartnerPromoCodeLimitAsync(Guid id, DateTime endDate, int limit);
		public Task CancelPartnerPromoCodeLimitAsync(Guid id);
	}
}
