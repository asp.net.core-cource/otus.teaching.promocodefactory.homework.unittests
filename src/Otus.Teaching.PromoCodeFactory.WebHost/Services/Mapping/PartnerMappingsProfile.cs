﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Services.Contracts;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services.Mapping
{
	public class PartnerMappingsProfile : Profile
	{
		public PartnerMappingsProfile()
		{
			CreateMap<Partner, PartnerDto>();
			CreateMap<PartnerPromoCodeLimit, PartnerPromoCodeLimitDto>();
		}
	}
}
