﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Services.Contracts;
using Otus.Teaching.PromoCodeFactory.WebHost.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
	public class PartnersService : IPartnersService
	{
		private readonly IMapper _mapper;
		private readonly IRepository<Partner> _partnersRepository;
		public PartnersService(IMapper mapper, IRepository<Partner> partnersRepository)
		{
			_mapper = mapper;
			_partnersRepository = partnersRepository;
		}
		public async Task<List<PartnerDto>> GetPartnersAsync()
		{
			var partners = await _partnersRepository.GetAllAsync();

			var result = partners.Select(p => _mapper.Map<Partner, PartnerDto>(p)).ToList();

			return result;
		}
		public async Task<PartnerPromoCodeLimitDto> GetPartnerLimitAsync(Guid id, Guid limitId)
		{
			var partner = await _partnersRepository.GetByIdAsync(id);

			if(partner == null)
				throw new PartnerNotFoundException("Партнер не найден");

			var limit = partner.PartnerLimits
				.FirstOrDefault(x => x.Id == limitId);

			var result = _mapper.Map<PartnerPromoCodeLimit, PartnerPromoCodeLimitDto>(limit);

			return result;
		}
		public async Task<(Guid, Guid)> SetPartnerPromoCodeLimitAsync(Guid id, DateTime endDate, int limit)
		{
            var partner = await _partnersRepository.GetByIdAsync(id);

            if(partner == null)
                throw new PartnerNotFoundException("Партнер не найден");

            //Если партнер заблокирован, то нужно выдать исключение
            if(!partner.IsActive)
                throw new PartnerNotActiveException("Данный партнер не активен");

            //Установка лимита партнеру
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
                !x.CancelDate.HasValue);

            if(activeLimit != null)
            {
                //Если партнеру выставляется лимит, то мы 
                //должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
                //то количество не обнуляется
                partner.NumberIssuedPromoCodes = 0;

                //При установке лимита нужно отключить предыдущий лимит
                activeLimit.CancelDate = DateTime.Now;
            }

            if(limit <= 0)
                throw new WrongLimitException("Лимит должен быть больше 0");

            var newLimit = new PartnerPromoCodeLimit()
            {
                Limit = limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = DateTime.Now,
                EndDate = endDate
            };

            partner.PartnerLimits.Add(newLimit);

            await _partnersRepository.UpdateAsync(partner);
            return (partner.Id, newLimit.Id);
        }
        public async Task CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);

            if(partner == null)
                throw new PartnerNotFoundException("Партнер не найден");

            //Если партнер заблокирован, то нужно выдать исключение
            if(!partner.IsActive)
                throw new PartnerNotActiveException("Данный партнер не активен");

            //Отключение лимита
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
                !x.CancelDate.HasValue);

            if(activeLimit != null)
            {
                activeLimit.CancelDate = DateTime.Now;
            }

            await _partnersRepository.UpdateAsync(partner);
        }
    }
}
